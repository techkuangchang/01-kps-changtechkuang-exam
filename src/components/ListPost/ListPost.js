import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getPosts } from '../../redux/action/postAction/postAction'

class ListPost extends Component {
    render() {
        
        console.log(this.props);
        this.props.getPosts()
        return (
            <div>
                
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        data : state.data
    }
}

const mapActionToProps = () => {
    return {getPosts}
}

export default connect(mapStateToProps, mapActionToProps)(ListPost)
