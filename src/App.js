import React from 'react';
import ListPost from './components/ListPost/ListPost';

function App() {
  return (
    <div className="App">
        <ListPost/>
    </div>
  );
}

export default App;
