import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import { postReducer } from './redux/action/reducer/postReducer';

const store = createStore(postReducer);

ReactDOM.render(
    <Provider store={store}>
    <React.StrictMode>
        <App/>
    </React.StrictMode>
    </Provider>,
    document.getElementById('root')
);